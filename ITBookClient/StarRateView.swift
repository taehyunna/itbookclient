//
//  StarRateView.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit

class StarRateView: UIView {
    
    private let countOfStars = 5
    private var maskImageViews:[UIImageView] = []
    private var fillStarView:UIView = UIView(frame: .zero)
    private var emptyStarView:UIView = UIView(frame: .zero)
    
    private func prepare() {
        
        addSubview(emptyStarView)
        addSubview(fillStarView)
        maskImageViews.removeAll()
        for _ in 0..<countOfStars {
            let imageView = UIImageView()
            maskImageViews.append(imageView)
            addSubview(imageView)
        }
        fillStarColor = UIColor(red: 0.9, green: 0.7, blue: 0.2, alpha: 1.0)
        emptyStarColor = .lightGray
    }
    
    var maskImage:UIImage? {
        didSet {
            for i in 0..<maskImageViews.count {
                maskImageViews[i].image = maskImage
            }
        }
    }
    
    var fillStarColor:UIColor? {
        get {
            return fillStarView.backgroundColor
        }
        set {
            fillStarView.backgroundColor = newValue
        }
    }
    
    var emptyStarColor:UIColor? {
        get {
            return emptyStarView.backgroundColor
        }
        set {
            emptyStarView.backgroundColor = newValue
        }
    }
    
    var rateValue:Float = 0 {
        didSet {
            rateValue = ((rateValue < 0 ? 0 : rateValue) > Float(countOfStars) ? Float(countOfStars) : rateValue)
            setNeedsLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        prepare()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        prepare()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        let starLength = bounds.size.width / CGFloat(countOfStars)
        let rate = CGFloat(rateValue) / CGFloat(countOfStars)
        emptyStarView.frame = CGRect(x: 0, y: (bounds.size.height-starLength)*0.5, width: starLength*CGFloat(countOfStars), height: starLength)
        fillStarView.frame = CGRect(x: 0, y: (bounds.size.height-starLength)*0.5, width: starLength*CGFloat(countOfStars)*rate, height:starLength)
        for i in 0..<maskImageViews.count {
            maskImageViews[i].frame = CGRect(x: CGFloat(i)*starLength, y: (bounds.size.height-starLength)*0.5, width: starLength, height: starLength)
        }
    }
}
