//
//  ITBookDogma.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit
import HJRestClientManager
import ObjectMapper

class ITBookClientDogma: HJRestClientDogma {
    
    fileprivate func modelFromObject(object:[String:Any], responseModelRefer: Any?) -> Any? {
        
        if let jsonModelClass = responseModelRefer as? Mappable.Type {
            if let model = jsonModelClass.init(JSON: object) {
                return model
            }
        }
        return object
    }
    
    func bodyFromRequestModel(_ requestModel:Any?) -> Data? {
        
        if let jsonModel = requestModel as? Mappable, let jsonString = jsonModel.toJSONString() {
            return jsonString.data(using: .utf8)
        }
        
        return requestModel as? Data
    }
    
    func contentTypeFromRequestModel(_ requestModel:Any?) -> String? {
        
        if (requestModel as? Mappable) != nil {
            return "application/json"
        }
        
        return "application/octet-stream"
    }
    
    func responseModel(fromData data: Data, serverAddress: String, endpoint: String?, contentType:String?, requestModel: Any?, responseModelRefer: Any?) -> Any? {
        
        guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) else {
            return nil
        }
        
        if let list = jsonObject as? [Any] {
            var modelList:[Any] = []
            for item in list {
                if let item = item as? [String:Any], let model = modelFromObject(object: item, responseModelRefer: responseModelRefer) {
                    modelList.append(model)
                }
            }
            return modelList
        }
        if let item = jsonObject as? [String:Any] {
            return modelFromObject(object: item, responseModelRefer: responseModelRefer)
        }
        
        return data
    }
    
    func responseData(fromModel model: Any, serverAddress: String, endpoint: String?, requestModel: Any?) -> Data? {
        
        if let jsonModel = model as? Mappable, let jsonString = jsonModel.toJSONString() {
            return jsonString.data(using: .utf8)
        }
        
        return model as? Data
    }
    
    func didReceiveResponse(response: URLResponse, serverAddress: String, endpoint: String?) {
        
        print("- did receive response from \(serverAddress)\(endpoint ?? "")")
    }
}
