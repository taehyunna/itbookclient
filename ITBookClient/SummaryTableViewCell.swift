//
//  SummaryTableViewCell.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit
import P9TableViewHandler

class SummaryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var summaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension SummaryTableViewCell: P9TableViewCellProtocol {
    
    static func cellHeightForData(_ data: Any?, extra: Any?) -> CGFloat {
        
        guard let data = data as? [String:Any], let summary = data["summary"] as? String, summary.count > 0 else {
            return 0
        }
        
        let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 40
        let titleBounds = summary.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                         attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13)],
                                            context: nil)
        return (titleBounds.size.height + 30)
    }
    
    func setData(_ data: Any?, extra: Any?) {
        
        guard let data = data as? [String:Any], let summary = data["summary"] as? String, summary.count > 0 else {
            summaryLabel.text = nil
            return
        }
        
        summaryLabel.text = summary
    }
}
