//
//  BookRecordTableViewCell.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit
import P9TableViewHandler
import UI_HJResourceManager

class BookRecordTableViewCell: UITableViewCell {
    
    fileprivate var data:BookRecordModel?
    fileprivate var touchBeganAnimating:Bool = false
    fileprivate var touchEndedCalled:Bool = false
    fileprivate var touchCanceledCalled:Bool = false
    fileprivate let placeHolderImage = UIImage(named: "empty")
    fileprivate weak var delegate:P9TableViewCellDelegate?
    
    @IBOutlet weak var boardView: UIView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var subtitleLabelHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var isbn13Label: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var underlineView: UIView!
    
    fileprivate func feedbackLandingEvent() {
        
        let thumbnailFrame = convert(thumbnailImageView.frame, to: nil)
        delegate?.tableViewCellEvent(cellIdentifier: BookRecordTableViewCell.identifier(), eventIdentifier: "landing", data: self.data, extra: thumbnailFrame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        touchBeganAnimating = true
        UIView.animate(withDuration: 0.3, animations: {
            self.boardView.transform = CGAffineTransform.init(scaleX: 0.94, y: 0.94)
        }) { (finished) in
            self.touchBeganAnimating = false
            if self.touchEndedCalled == true || self.touchCanceledCalled == true {
                UIView.animate(withDuration: 0.3, animations: {
                    self.boardView.transform = .identity
                }) { (finished) in
                    if self.touchEndedCalled == true {
                        self.feedbackLandingEvent()
                    }
                    self.touchEndedCalled = false
                    self.touchEndedCalled = false
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if touchBeganAnimating == false {
            UIView.animate(withDuration: 0.3, animations: {
                self.boardView.transform = .identity
            }) { (finished) in
                self.feedbackLandingEvent()
            }
        } else {
            touchEndedCalled = true
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        if touchBeganAnimating == false {
            UIView.animate(withDuration: 0.3, animations: {
                self.boardView.transform = .identity
            })
        } else {
            touchCanceledCalled = true
        }
    }
}

extension BookRecordTableViewCell: P9TableViewCellProtocol {
    
    static func cellHeightForData(_ data: Any?, extra: Any?) -> CGFloat {
        
        guard (data as? BookRecordModel) != nil else {
            return 0
        }
        return 120
    }
    
    func setData(_ data: Any?, extra: Any?) {
        
        guard let data = data as? BookRecordModel else {
            titleLabel.attributedText = nil
            titleLabelHeightConstraints.constant = 0
            subtitleLabel.text = nil
            subtitleLabelHeightConstraints.constant = 0
            priceLabel.text = nil
            thumbnailImageView.image = nil
            return
        }
        
        self.data = data
        
        if let title = data.title, title.count > 0 {
            let attributedText = NSMutableAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
            if let extra = extra as? [String:Any], let keyword = extra["keyword"] as? String, keyword.count > 0 {
                let range = (title as NSString).range(of: keyword, options: [.caseInsensitive])
                if range.location != NSNotFound {
                    attributedText.addAttributes([NSAttributedString.Key.foregroundColor:UIColor.blue], range: range)
                }
            }
            titleLabel.attributedText = attributedText
            let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 130
            let titleBounds = title.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                              options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                           attributes: [NSAttributedString.Key.font: titleLabel.font!],
                                              context: nil)
            titleLabelHeightConstraints.constant = min(36, (titleBounds.size.height + 4))
        } else {
            titleLabel.attributedText = nil
            titleLabelHeightConstraints.constant = 0
        }
        if let subtitle = data.subtitle, subtitle.count > 0 {
            subtitleLabel.text = subtitle
            let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 130
            let titleBounds = subtitle.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                              attributes: [NSAttributedString.Key.font: subtitleLabel.font!],
                                                 context: nil)
            subtitleLabelHeightConstraints.constant = min(32, (titleBounds.size.height + 4))
        } else {
            subtitleLabel.text = nil
            subtitleLabelHeightConstraints.constant = 0
        }
        if let isbn13 = data.isbn13 {
            isbn13Label.text = "ISBN-13 : \(isbn13)"
        } else {
            isbn13Label.text = nil
        }
        priceLabel.text = data.price
        if let imageUrl = data.image {
            thumbnailImageView.setImageUrl(imageUrl, placeholderImage: placeHolderImage, cutInLine: true)
        } else {
            thumbnailImageView.image = nil
        }
        if let extra = extra as? [String:Any], let isLast = extra["isLast"] as? Bool, isLast == true {
            underlineView.isHidden = true
        } else {
            underlineView.isHidden = false
        }
    }
    
    func setDelegate(_ delegate: P9TableViewCellDelegate) {
        
        self.delegate = delegate
    }
}
