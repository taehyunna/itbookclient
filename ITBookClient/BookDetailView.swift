//
//  BookDetailView.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit


protocol BookDetailViewDelegate: class {
    
    func bookDetailViewCloseButtonTouchUpInside(_ bookDetailView:BookDetailView)
}

class BookDetailView: UIView {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: BookDetailViewDelegate?
    
    static func instanceFromNib() -> BookDetailView {
        
        let view = Bundle.main.loadNibNamed("BookDetailView", owner: nil, options: nil)?[0] as! BookDetailView
        view.headerView.layer.shadowColor = UIColor.black.cgColor
        view.headerView.layer.shadowOffset = CGSize(width: 0, height: -4)
        view.headerView.layer.shadowOpacity = 0.5
        return view
    }
    
    func suggestThumbnailImageFrame() -> CGRect {
        
        let length = UIScreen.main.bounds.size.width
        return convert(CGRect(x: 0, y: 0, width: length, height: length), to: nil)
    }
    
    @IBAction func closeButtonTouchUpInside(_ sender: Any) {
        
        delegate?.bookDetailViewCloseButtonTouchUpInside(self)
    }
    
}
