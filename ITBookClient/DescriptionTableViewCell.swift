//
//  DescriptionTableViewCell.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit
import P9TableViewHandler

class DescriptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionLabelHeightConstraints: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension DescriptionTableViewCell: P9TableViewCellProtocol {
    
    static func cellHeightForData(_ data: Any?, extra: Any?) -> CGFloat {
        
        guard let data = data as? [String:Any], let title = data["title"] as? String, title.count > 0 else {
            return 0
        }
        
        var height:CGFloat = 10
        if let desc = data["desc"] as? String, desc.count > 0 {
            let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 120
            let titleBounds = desc.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                             options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                          attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12)],
                                                 context: nil)
            height += (titleBounds.size.height + 4)
        }
        
        return height
    }
    
    func setData(_ data: Any?, extra: Any?) {
        
        guard let data = data as? [String:Any], let title = data["title"] as? String, title.count > 0 else {
            titleLabel.text = nil
            descriptionLabel.text = nil
            descriptionLabelHeightConstraints.constant = 0
            return
        }
        
        titleLabel.text = title
        if let desc = data["desc"] as? String, desc.count > 0 {
            descriptionLabel.text = desc
            let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 120
            let titleBounds = desc.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                             options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                          attributes: [NSAttributedString.Key.font: descriptionLabel.font!],
                                                 context: nil)
            descriptionLabelHeightConstraints.constant = titleBounds.size.height + 4
        } else {
            descriptionLabel.text = nil
            descriptionLabelHeightConstraints.constant = 0
        }
        if let bgflag = extra as? Bool, bgflag == true {
            contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
        } else {
            contentView.backgroundColor = .white
        }
    }
}
