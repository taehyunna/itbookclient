//
//  CoverImageTableViewCell.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit
import P9TableViewHandler

class CoverImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension CoverImageTableViewCell: P9TableViewCellProtocol {
    
    static func cellHeightForData(_ data: Any?, extra: Any?) -> CGFloat {
        
        return UIScreen.main.bounds.size.width
    }
    
    func setData(_ data: Any?, extra: Any?) {
        
        guard let urlString = data as? String, urlString.count > 0 else {
            coverImageView.image = nil
            return
        }
        coverImageView.setImageUrl(urlString, placeholderImage: nil, cutInLine: true)
    }
}
