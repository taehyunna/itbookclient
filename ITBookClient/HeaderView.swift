//
//  HeaderView.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit

class HeaderView: UIView {
    
    static let heightForTitleOnly:CGFloat = 40
    static let heightForSearch:CGFloat = 85
    
    @IBOutlet weak var keywordTextField: UITextField!
    
    static func instanceFromNib() -> HeaderView {
        
        return Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)?[0] as! HeaderView
    }
}
