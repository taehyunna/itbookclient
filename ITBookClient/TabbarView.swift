//
//  TabbarView.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit

protocol TabbarViewDelegate: class {
    
    func tabBarViewDidSelectTab(_ tabbarView:TabbarView, tab:TabbarView.Tab)
}

class TabbarView: UIView {
    
    enum Tab {
        case New
        case Search
    }
    
    @IBOutlet weak var newButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    weak var delegate: TabbarViewDelegate?
    
    static func instanceFromNib() -> TabbarView {
        
        return Bundle.main.loadNibNamed("TabbarView", owner: nil, options: nil)?[0] as! TabbarView
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
        print("gotta")
    }
    
    var selectedTab:Tab = .New {
        didSet {
            newButton.isSelected = (selectedTab == .New)
            searchButton.isSelected = (selectedTab == .Search)
            delegate?.tabBarViewDidSelectTab(self, tab: selectedTab)
        }
    }
    
    @IBAction func newButtonTouchUpInside(_ sender: Any) {
        
        selectedTab = .New
        delegate?.tabBarViewDidSelectTab(self, tab: selectedTab)
    }
    
    @IBAction func searchButtonTouchUpInside(_ sender: Any) {
        
        selectedTab = .Search
        delegate?.tabBarViewDidSelectTab(self, tab: selectedTab)
    }
}
