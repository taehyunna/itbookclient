//
//  LobbyViewController.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit
import HJRestClientManager
import P9TableViewHandler
import P9ViewDragger

class LobbyViewController: UIViewController {
    
    private typealias SearchPageInfo = (keyword:String, lastPage:Int, requestedPage:Int, currentRecords:Int, totalRecords:Int)
    
    private let bookListCellIdentifierForType:[String:String] = [
        "r" : BookRecordTableViewCell.identifier()
    ]
    private let bookDetailCellIdentifierForType:[String:String] = [
        "cover"   : CoverImageTableViewCell.identifier(),
        "stall"   : StallTableViewCell.identifier(),
        "desc"    : DescriptionTableViewCell.identifier(),
        "summary" : SummaryTableViewCell.identifier()
    ]
    
    private let dogmaKey = "itbook"
    private let cacheExpireIntervnal:TimeInterval = (60*60*3)
    
    private let newBookIdentifier = "new"
    private let searchBookIdentifier = "search"
    private let detailBookIdentifier = "detail"
    
    private let newBookHandler:P9TableViewHandler = P9TableViewHandler()
    private let searchBookHandler:P9TableViewHandler = P9TableViewHandler()
    private let detailBookHandler:P9TableViewHandler = P9TableViewHandler()
    private var newBooksSections:[P9TableViewHandler.Section] = []
    private var searchBooksSections:[P9TableViewHandler.Section] = []
    private var detailBookSections:[P9TableViewHandler.Section] = []
    private var transformStack:[CGAffineTransform] = []
    private var searchPageInfo:SearchPageInfo = ("", 0, 0, 0, 0)
    private var requestingMorePage = false
    
    private let refreshNewBookControl = UIRefreshControl()
    private let newBookTableView = UITableView()
    private let searchBookTableView = UITableView()
    private let headerView = HeaderView.instanceFromNib()
    private let tabbarView = TabbarView.instanceFromNib()
    private let detailView = BookDetailView.instanceFromNib()
    
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var headerContainerHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var contentsContainerView: UIView!
    @IBOutlet weak var tabContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        headerView.keywordTextField.delegate = self
        headerView.frame = headerContainerView.bounds
        headerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        headerContainerView.autoresizesSubviews = true
        headerContainerView.addSubview(headerView)
        
        tabbarView.delegate = self
        tabbarView.frame = tabContainerView.bounds
        tabbarView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tabContainerView.autoresizesSubviews = true
        tabContainerView.addSubview(tabbarView)
        
        refreshNewBookControl.addTarget(self, action: #selector(refreshNewBookList(refresh:)), for: .valueChanged)
        
        newBookTableView.contentInsetAdjustmentBehavior = .never
        newBookTableView.separatorStyle = .none
        newBookTableView.frame = contentsContainerView.bounds
        newBookTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        newBookTableView.autoresizesSubviews = true
        contentsContainerView.addSubview(newBookTableView)
        
        newBookTableView.refreshControl = refreshNewBookControl
        
        newBookHandler.delegate = self
        newBookHandler.standby(identifier:newBookIdentifier, cellIdentifierForType: bookListCellIdentifierForType, tableView: newBookTableView)
        newBookHandler.setSections(newBooksSections)
        
        searchBookTableView.contentInsetAdjustmentBehavior = .never
        searchBookTableView.separatorStyle = .none
        searchBookTableView.frame = contentsContainerView.bounds
        searchBookTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        searchBookTableView.autoresizesSubviews = true
        contentsContainerView.addSubview(searchBookTableView)
        
        searchBookHandler.delegate = self
        searchBookHandler.standby(identifier:searchBookIdentifier, cellIdentifierForType: bookListCellIdentifierForType, tableView: searchBookTableView)
        searchBookHandler.setSections(searchBooksSections)
        
        detailView.delegate = self
        detailView.frame = CGRect(x: 0, y: 51, width: view.bounds.size.width, height: view.bounds.size.height-51)
        detailView.isHidden = true
        view.addSubview(detailView)
        
        detailBookHandler.delegate = self
        detailBookHandler.standby(identifier:detailBookIdentifier, cellIdentifierForType: bookDetailCellIdentifierForType, tableView: detailView.tableView)
        detailBookHandler.setSections(detailBookSections)
        
        P9ViewDragger.default().trackingView(detailView, parameters: [P9ViewDraggerLockScaleKey:true, P9ViewDraggerLockRotateKey:true], ready: nil, trackingHandler: { (trackingView) in
            var transform = trackingView.transform
            if transform.ty < 0 {
                transform.ty = 0
            }
            transform.tx = 0
            trackingView.transform = transform
        }, completion: { (trackingView) in
            if trackingView.transform.ty > self.detailView.bounds.size.height * 0.3 {
                self.closeDetailBookView()
            } else {
                self.bringBackDetailBookView()
            }
        })
        
        tabbarView.selectedTab = .New
    }
    
    @objc func refreshNewBookList(refresh: UIRefreshControl) {
        
        requestNew()
    }
}

extension LobbyViewController {
    
    fileprivate func toggleSearchMode(_ searchMode:Bool) {
        
        headerContainerHeightConstraints.constant = (searchMode ? HeaderView.heightForSearch : HeaderView.heightForTitleOnly)
        UIView.animate(withDuration: 0.3) {
            self.headerView.keywordTextField.alpha = (searchMode ? 1 : 0)
            self.view.layoutIfNeeded()
        }
        if searchMode == true {
            headerView.keywordTextField.becomeFirstResponder()
        }
    }
    
    fileprivate func openDetailBookView(data:BookRecordModel, isbn13:String, fromFrame targetFrame:CGRect?) {
        
        detailBookSections.removeAll()
        var records:[P9TableViewHandler.Record] = []
        if let image = data.image, image.count > 0 {
            records.append(P9TableViewHandler.Record(type: "cover", data: image, extra: nil))
        }
        if let title = data.title, title.count > 0, let subtitle = data.subtitle, subtitle.count > 0 {
            records.append(P9TableViewHandler.Record(type: "stall", data: ["title":title, "subtitle":subtitle], extra: nil))
        }
        detailBookSections.append(P9TableViewHandler.Section(headerType: nil, headerData: nil, footerType: nil, footerData: nil, records: records, extra: nil))
        detailBookHandler.setSections(detailBookSections)
        detailView.tableView.reloadData()
        
        if let targetFrame = targetFrame {
            let rate = targetFrame.size.width / detailView.bounds.size.width
            let tx = targetFrame.midX - detailView.center.x
            let ty = targetFrame.midY - (detailView.center.y - ((detailView.bounds.size.height-detailView.bounds.size.width)*rate*0.5)) - 10
            let transform = CGAffineTransform.init(translationX: tx, y: ty).scaledBy(x: rate, y: rate)
            transformStack.append(transform)
            detailView.transform = transform
        }
        detailView.alpha = 1
        detailView.isHidden = false
        requestDetail(isbn13: isbn13)
        UIView.animate(withDuration: 0.3, animations: {
            self.detailView.alpha = 1
            self.detailView.transform = .identity
        }) { (finished) in

        }
    }
    
    fileprivate func closeDetailBookView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.detailView.alpha = 0
            if let last = self.transformStack.last {
                self.detailView.transform = last
                self.transformStack.removeLast()
            }
        }) { (finished) in
            self.detailView.isHidden = true
            self.detailView.transform = .identity
        }
    }
    
    fileprivate func bringBackDetailBookView() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.detailView.transform = .identity
        }) { (finished) in
            
        }
    }
}

extension LobbyViewController {
    
    func updateNewBook(fromModel model:BookListResponseModel) {
        
        newBooksSections.removeAll()
        if let total = model.total, let n = Int(total), n > 0, let books = model.books {
            var records:[P9TableViewHandler.Record] = []
            for (index, book) in books.enumerated() {
                records.append(P9TableViewHandler.Record(type: "r", data: book, extra: ["isLast":(index == books.count-1)]))
            }
            self.newBooksSections.append(P9TableViewHandler.Section(headerType: nil, headerData: nil, footerType: nil, footerData: nil, records: records, extra: nil))
        }
        self.newBookHandler.setSections(self.newBooksSections)
        self.newBookTableView.reloadData()
    }
    
    func updateSearchBook(fromModel model:BookListResponseModel?, requestedPage:Int?, keyword:String?) {
        
        guard let model = model, let requestedPage = requestedPage, let keyword = keyword else {
            searchPageInfo = ("", 0, 0, 0, 0)
            searchBooksSections.removeAll()
            searchBookHandler.setSections(searchBooksSections)
            searchBookTableView.reloadData()
            return
        }
        
        if let total = model.total, let n = Int(total), n > 0, let books = model.books {
            self.searchPageInfo.lastPage = requestedPage
            self.searchPageInfo.requestedPage = requestedPage
            self.searchPageInfo.currentRecords = self.searchPageInfo.currentRecords + books.count
            self.searchPageInfo.totalRecords = n
            requestingMorePage = false
            var records:[P9TableViewHandler.Record] = []
            for (index, book) in books.enumerated() {
                records.append(P9TableViewHandler.Record(type: "r", data: book, extra: ["isLast":(index == books.count-1), "keyword":keyword]))
            }
            self.searchBooksSections.append(P9TableViewHandler.Section(headerType: nil, headerData: nil, footerType: nil, footerData: nil, records: records, extra: nil))
        }
        self.searchBookHandler.setSections(self.searchBooksSections)
        self.searchBookTableView.reloadData()
    }
    
    func updateDetailBook(fromModel model:BookDetailResponseModel) {
        
        detailBookSections.removeAll()
        var records:[P9TableViewHandler.Record] = []
        if let image = model.image, image.count > 0 {
            records.append(P9TableViewHandler.Record(type: "cover", data: image, extra: nil))
        }
        if let title = model.title, title.count > 0, let subtitle = model.subtitle, subtitle.count > 0, let price = model.price, price.count > 0, let rating = model.rating, rating.count > 0 {
            records.append(P9TableViewHandler.Record(type: "stall", data: ["title":title, "subtitle":subtitle, "price":price, "rating":rating], extra: nil))
        }
        var descIndex = 0
        if let authors = model.authors, authors.count > 0 {
            records.append(P9TableViewHandler.Record(type: "desc", data: ["title":"Author", "desc":authors], extra: (descIndex % 2 == 0)))
            descIndex += 1
        }
        if let publisher = model.publisher, publisher.count > 0 {
            records.append(P9TableViewHandler.Record(type: "desc", data: ["title":"Publisher", "desc":publisher], extra: (descIndex % 2 == 0)))
            descIndex += 1
        }
        if let pages = model.pages, pages.count > 0 {
            records.append(P9TableViewHandler.Record(type: "desc", data: ["title":"Pages", "desc":pages], extra: (descIndex % 2 == 0)))
            descIndex += 1
        }
        if let language = model.language, language.count > 0 {
            records.append(P9TableViewHandler.Record(type: "desc", data: ["title":"Language", "desc":language], extra: (descIndex % 2 == 0)))
            descIndex += 1
        }
        if let year = model.year, year.count > 0 {
            records.append(P9TableViewHandler.Record(type: "desc", data: ["title":"Year", "desc":year], extra: (descIndex % 2 == 0)))
            descIndex += 1
        }
        if let isbn10 = model.isbn10, isbn10.count > 0 {
            records.append(P9TableViewHandler.Record(type: "desc", data: ["title":"ISBN-10", "desc":isbn10], extra: (descIndex % 2 == 0)))
            descIndex += 1
        }
        if let isbn13 = model.isbn13, isbn13.count > 0 {
            records.append(P9TableViewHandler.Record(type: "desc", data: ["title":"ISBN-13", "desc":isbn13], extra: (descIndex % 2 == 0)))
            descIndex += 1
        }
        if let summary = model.desc, summary.count > 0 {
            records.append(P9TableViewHandler.Record(type: "summary", data: ["summary":summary], extra: nil))
        }
        self.detailBookSections.append(P9TableViewHandler.Section(headerType: nil, headerData: nil, footerType: nil, footerData: nil, records: records, extra: nil))
        self.detailBookHandler.setSections(self.detailBookSections)
        self.detailView.tableView.reloadData()
    }
    
    func requestNew() {
        
        HJRestClientManager.request().endpoint("/new").responseModelRefer(BookListResponseModel.self).resume { [weak self] (result:[String : Any]?) -> [String : Any]? in
            guard let self = self else {
                return nil
            }
            self.refreshNewBookControl.endRefreshing()
            guard let model = result?[HJRestClientManager.NotificationResponseModel] as? BookListResponseModel, let error = model.error, error == "0" else {
                return nil
            }
            self.updateNewBook(fromModel: model)
            return nil
        }
    }
    
    func requestSearch(keyword:String, page:Int?=nil) {
        
        let encodedKeyword = keyword.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if encodedKeyword.count <= 0 {
            return
        }
        var boundedPage = 1
        if let page = page, page > 0 {
            boundedPage = page
        }
        
        if boundedPage == 1 {
            searchBooksSections.removeAll()
            searchPageInfo.lastPage = 0
            searchPageInfo.requestedPage = boundedPage
            searchPageInfo.currentRecords = 0
        }
        searchPageInfo.keyword = keyword
        searchPageInfo.requestedPage = boundedPage
        
        let endpoint = "/search/\(encodedKeyword)/\(boundedPage)"
        
        if let model = HJRestClientManager.shared.cachedResponseModel(method: .get, endpoint: endpoint, requestModel: nil, responseModelRefer: BookListResponseModel.self, dogmaKey: dogmaKey, expireTimeInterval: cacheExpireIntervnal) as? BookListResponseModel {
            self.updateSearchBook(fromModel: model, requestedPage: boundedPage, keyword: keyword)
            return
        }
        
        HJRestClientManager.request().endpoint(endpoint).responseModelRefer(BookListResponseModel.self).updateCache(true).resume { [weak self] (result:[String : Any]?) -> [String : Any]? in
            guard let self = self, keyword == self.searchPageInfo.keyword else {
                return nil
            }
            guard let model = result?[HJRestClientManager.NotificationResponseModel] as? BookListResponseModel, let error = model.error, error == "0" else {
                self.searchPageInfo.requestedPage = self.searchPageInfo.lastPage
                return nil
            }
            self.updateSearchBook(fromModel: model, requestedPage: boundedPage, keyword: keyword)
            return nil
        }
    }
    
    func requestDetail(isbn13:String) {
        
        let endpoint = "/books/\(isbn13)"
        
        HJRestClientManager.request().endpoint(endpoint).responseModelRefer(BookDetailResponseModel.self).resume { [weak self] (result:[String : Any]?) -> [String : Any]? in
            guard let self = self, let model = result?[HJRestClientManager.NotificationResponseModel] as? BookDetailResponseModel, let error = model.error, error == "0" else {
                return nil
            }
            self.updateDetailBook(fromModel: model)
            return nil
        }
    }
}

extension LobbyViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text, text.count > 50 {
            return false
        }
        
        var keyword = ""
        if let text = textField.text, let textRange = Range(range, in: text) {
            keyword = text.replacingCharacters(in: textRange, with: string)
        }
        if keyword.count > 0 {
            requestSearch(keyword: keyword, page: 1)
        } else {
            updateSearchBook(fromModel: nil, requestedPage: 0, keyword: nil)
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        updateSearchBook(fromModel: nil, requestedPage: 0, keyword: nil)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if headerView.keywordTextField.isFirstResponder {
            headerView.keywordTextField.resignFirstResponder()
        }
        return false
    }
}

extension LobbyViewController: TabbarViewDelegate {
    
    func tabBarViewDidSelectTab(_ tabbarView: TabbarView, tab: TabbarView.Tab) {
        
        switch tab {
        case .New :
            newBookTableView.isHidden = false
            searchBookTableView.isHidden = true
            if newBooksSections.count == 0 {
                requestNew()
            }
        case .Search :
            newBookTableView.isHidden = true
            searchBookTableView.isHidden = false
        }
        toggleSearchMode(tab == .Search)
    }
}

extension LobbyViewController: BookDetailViewDelegate {
    
    func bookDetailViewCloseButtonTouchUpInside(_ bookDetailView: BookDetailView) {
        
        closeDetailBookView()
    }
}

extension LobbyViewController: P9TableViewHandlerDelegate {
    
    func tableViewHandlerCellEvent(handlerIdentifier: String, cellIdentifier:String, eventIdentifier:String?, data: Any?, extra: Any?) {
        
        if headerView.keywordTextField.isFirstResponder {
            headerView.keywordTextField.resignFirstResponder()
        }
        
        switch cellIdentifier {
        case BookRecordTableViewCell.identifier() :
            if let eventIdentifier = eventIdentifier, eventIdentifier == "landing", let data = data as? BookRecordModel, let isbn13 = data.isbn13, isbn13.count > 0 {
                openDetailBookView(data: data, isbn13: isbn13, fromFrame: extra as? CGRect)
            }
        default :
            break
        }
    }
    
    func tableViewHandlerDidScroll(handlerIdentifier: String, contentSize: CGSize, contentOffset: CGPoint) {
        
        if headerView.keywordTextField.isFirstResponder {
            headerView.keywordTextField.resignFirstResponder()
        }
        if handlerIdentifier != searchBookIdentifier {
            return
        }
        if searchPageInfo.currentRecords >= searchPageInfo.totalRecords {
            return
        }
        if contentOffset.y > (contentSize.height - (searchBookTableView.bounds.size.height*2)) {
            if requestingMorePage == false, searchPageInfo.lastPage >= searchPageInfo.requestedPage {
                requestingMorePage = true
                requestSearch(keyword: searchPageInfo.keyword, page: searchPageInfo.lastPage+1)
            }
        }
    }
}
