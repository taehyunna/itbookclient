//
//  StallTableViewCell.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit
import P9TableViewHandler

class StallTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var subTitleLabelHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ratingView: StarRateView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ratingView.maskImage = UIImage(named: "starMask")
    }
}

extension StallTableViewCell: P9TableViewCellProtocol {
    
    static func cellHeightForData(_ data: Any?, extra: Any?) -> CGFloat {
        
        guard let data = data as? [String:Any] else {
            return 0
        }
        
        var height:CGFloat = 45
        if let title = data["title"] as? String, title.count > 0 {
            let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 40
            let titleBounds = title.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                              options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                           attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)],
                                              context: nil)
            height += (titleBounds.size.height + 4)
        }
        if let subtitle = data["subtitle"] as? String, subtitle.count > 0 {
            let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 40
            let titleBounds = subtitle.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                              attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)],
                                                 context: nil)
            height += (titleBounds.size.height + 4)
        }
        
        return height
    }
    
    func setData(_ data: Any?, extra: Any?) {
        
        guard let data = data as? [String:Any] else {
            titleLabel.text = nil
            titleLabelHeightConstraints.constant = 0
            subTitleLabel.text = nil
            subTitleLabelHeightConstraints.constant = 0
            priceLabel.text = nil
            ratingView.rateValue = 0
            return
        }
        
        if let title = data["title"] as? String, title.count > 0 {
            titleLabel.text = title
            let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 40
            let titleBounds = title.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                              options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                           attributes: [NSAttributedString.Key.font: titleLabel.font!],
                                              context: nil)
            titleLabelHeightConstraints.constant = titleBounds.size.height + 4
        } else {
            titleLabel.text = nil
            titleLabelHeightConstraints.constant = 0
        }
        if let subtitle = data["subtitle"] as? String, subtitle.count > 0 {
            subTitleLabel.text = subtitle
            let contentsWidth:CGFloat = UIScreen.main.bounds.size.width - 40
            let titleBounds = subtitle.boundingRect(with: CGSize(width: contentsWidth, height: CGFloat.greatestFiniteMagnitude),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                              attributes: [NSAttributedString.Key.font: subTitleLabel.font!],
                                                 context: nil)
            subTitleLabelHeightConstraints.constant = titleBounds.size.height + 4
        } else {
            subTitleLabel.text = nil
            subTitleLabelHeightConstraints.constant = 0
        }
        priceLabel.text = data["price"] as? String ?? ""
        ratingView.rateValue = Float(data["rating"] as? String ?? "0") ?? 0
    }
}
