//
//  BookRecordModel.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import Foundation
import ObjectMapper

class BookRecordModel: Mappable {
    
    var title: String?
    var subtitle: String?
    var isbn13: String?
    var price: String?
    var image: String?
    var url: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title    <- map["title"]
        subtitle <- map["subtitle"]
        isbn13   <- map["isbn13"]
        price    <- map["price"]
        image    <- map["image"]
        url      <- map["url"]
    }
}
