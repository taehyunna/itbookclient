//
//  BookListResponseModel.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import Foundation
import ObjectMapper

class BookListResponseModel: Mappable {
    
    var error: String?
    var total: String?
    var page: String?
    var books: [BookRecordModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        total <- map["total"]
        page  <- map["page"]
        books <- map["books"]
    }
}
