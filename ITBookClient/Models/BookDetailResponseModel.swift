//
//  BookDetailResponseModel.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import Foundation
import ObjectMapper

class BookDetailResponseModel: Mappable {
    
    var error: String?
    var title: String?
    var subtitle: String?
    var authors: String?
    var publisher: String?
    var language: String?
    var isbn10: String?
    var isbn13: String?
    var pages: String?
    var year: String?
    var rating: String?
    var desc: String?
    var price: String?
    var image: String?
    var url: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error     <- map["error"]
        title     <- map["title"]
        subtitle  <- map["subtitle"]
        authors   <- map["authors"]
        publisher <- map["publisher"]
        language  <- map["language"]
        isbn10    <- map["isbn10"]
        isbn13    <- map["isbn13"]
        pages     <- map["pages"]
        year      <- map["year"]
        rating    <- map["rating"]
        desc      <- map["desc"]
        price     <- map["price"]
        image     <- map["image"]
        url       <- map["url"]
    }
}
