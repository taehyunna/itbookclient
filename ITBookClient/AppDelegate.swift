//
//  AppDelegate.swift
//  ITBookClient
//
//  Created by Tae Hyun Na

import UIKit
import HJResourceManager
import HJRestClientManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Hydra.default().addNormalWorker(forName: "hjrm")
        Hydra.default().addNormalWorker(forName: "hjrc")
        
        let hjrmRepoPath = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/hjrm"
        if HJResourceManager.default().standby(withRepositoryPath: hjrmRepoPath, localJobWorkerName: "hjrm", remoteJobWorkerName: "hjrm") {
            HJResourceManager.default().bind(toHydra: Hydra.default())
        }
        let hjrcRepoPath = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/hjrc"
        if HJRestClientManager.shared.standby(withRepositoryPath: hjrcRepoPath, workerName: "hjrc") == true {
            HJRestClientManager.shared.bind(toHydra: Hydra.default())
            HJRestClientManager.shared.setDogma(ITBookClientDogma(), forKey: "itbook")
            HJRestClientManager.shared.defaultDogmaKey = "itbook"
            HJRestClientManager.shared.setServerAddresses(["itbook":"https://api.itbook.store/1.0"])
            HJRestClientManager.shared.defaultServerKey = "itbook"
        }
        
        Hydra.default().startAllWorkers()
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

