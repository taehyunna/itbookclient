#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "HJResourceCipherBase64.h"
#import "HJResourceCipherCompress.h"
#import "HJResourceCipherGzip.h"
#import "HJResourceCipherProtocol.h"
#import "HJResourceCipherRsa.h"
#import "HJResourceCommon.h"
#import "HJResourceExecutorLocalJob.h"
#import "HJResourceExecutorRemoteJob.h"
#import "HJResourceManager.h"
#import "HJResourceRemakerProtocol.h"
#import "HJResourceRemakerResizeImage.h"

FOUNDATION_EXPORT double HJResourceManagerVersionNumber;
FOUNDATION_EXPORT const unsigned char HJResourceManagerVersionString[];

