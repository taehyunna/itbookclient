#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "P9ViewDragger.h"

FOUNDATION_EXPORT double P9ViewDraggerVersionNumber;
FOUNDATION_EXPORT const unsigned char P9ViewDraggerVersionString[];

