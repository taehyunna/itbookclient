#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "HJProgressViewProtocol.h"
#import "UIButton+HJResourceManager.h"
#import "UIImageView+HJResourceManager.h"

FOUNDATION_EXPORT double UI_HJResourceManagerVersionNumber;
FOUNDATION_EXPORT const unsigned char UI_HJResourceManagerVersionString[];

