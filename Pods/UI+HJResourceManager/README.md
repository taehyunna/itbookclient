UI+HJResourceManager
============

Asynchronous image downloader with cache support as a UIImageView/UIButton category, based on Hydra framework.

# Installation

You can download the latest framework files from our Release page.
UI+HJResourceManager also available through CocoaPods. To install it simply add the following line to your Podfile.
pod ‘UI+HJResourceManager’

# License

MIT License, where applicable. http://en.wikipedia.org/wiki/MIT_License
